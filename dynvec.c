/*
	dynamic vector
	Author : M.Langer
	Copyright : M.Langer

	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT OWNER AND CONTRIBUTORS AS IS
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <string.h>

#include "dynvec.h"

#define NUMBER_OF_CHUNKS(n)   (n / DYNVEC_CHUNK_SIZE)
#define FIT_SIZE_TO_CHUNK(s)  (s + DYNVEC_CHUNK_SIZE - (s % DYNVEC_CHUNK_SIZE))
#define IDX_TO_CHUNK(idx)     (idx / DYNVEC_CHUNK_SIZE)
#define IDX_TO_OFFSET(idx)    (idx % DYNVEC_CHUNK_SIZE)

/* init dynamic vector */
int dynvec_init(struct dynvec_t *v, unsigned int size)
{
	#ifdef DYNVEC_USE_SAFETY_CHECKS
	if(v == NULL)
		return -1;
	#endif

	/* pre-allocate chunks, if size > 0 */
	if(size > 0)
	{
		unsigned int i;

		/* round size to nearest number dividable by DYNVEC_CHUNK_SIZE */
		size = FIT_SIZE_TO_CHUNK(size);

		v->chunks = calloc(NUMBER_OF_CHUNKS(size), sizeof(struct chunk_t*));
		if(v->chunks == NULL)
			return -2;

		/* init chunk pointers */
		for(i = 0; i < NUMBER_OF_CHUNKS(size); i++)
			v->chunks[i] = NULL;
	}

	v->chunks = NULL;
	v->size = size;
	return 0;
}

/* delete dynamic vector, optionally call deletor on each item */
int dynvec_delete(struct dynvec_t *v, void(*del_func)(unsigned int idx, void* data))
{
	#ifdef DYNVEC_USE_SAFETY_CHECKS
	if(v == NULL)
		return -1;
	#endif

	/* delete data inside all chunks if deletor function is given */
	if((v->chunks != NULL) & (v->size > 0))
	{
		unsigned int i;

		for(i = 0; i < NUMBER_OF_CHUNKS(v->size); i++)
		{
			if(v->chunks[i] == NULL)
				continue;

			/* look for data to be deleted in non-empty chunk */
			unsigned int j;
			for(j = 0; j < DYNVEC_CHUNK_SIZE; j++)
			{
				if(v->chunks[i]->data[j] != NULL)
				{
					if(del_func != NULL)
						del_func(i * DYNVEC_CHUNK_SIZE + j, v->chunks[i]->data[j]);
					v->chunks[i]->data[j] = NULL;
				}
			}

			/* delete chunk (is now empty) */
			free(v->chunks[i]);
			v->chunks[i] = NULL;
		}
	}

	if(v->chunks != NULL)
		free(v->chunks);
	v->chunks = NULL;
	v->size = 0;
	return 0;
}

/* retrieve item at given index */
void* dynvec_get(struct dynvec_t *v, unsigned int idx)
{
	#ifdef DYNVEC_USE_SAFETY_CHECKS
	if(v == NULL)
		return NULL;
	if(v->chunks == NULL)
		return NULL;
	#endif
	if(idx >= v->size)
		return NULL;

	if(v->chunks[IDX_TO_CHUNK(idx)] == NULL)
		return NULL;
	return v->chunks[IDX_TO_CHUNK(idx)]->data[IDX_TO_OFFSET(idx)];
}

/* set item at given index */
int dynvec_set(struct dynvec_t *v, unsigned int idx, void* value)
{
	#ifdef DYNVEC_USE_SAFETY_CHECKS
	if(v == NULL)
		return -1;
	if(v->chunks == NULL)
		return -2;
	#endif
	if(idx >= v->size)
	{
		/* resize chunk array to hold at least 'idx' entries */
		unsigned int new_size = FIT_SIZE_TO_CHUNK(idx);
		void* new_arr = realloc(v->chunks, NUMBER_OF_CHUNKS(new_size) * sizeof(struct chunk_t*));
		if(new_arr == NULL)
			return -3;
		/* initialise new elements */
		memset((char*)new_arr + NUMBER_OF_CHUNKS(v->size) * sizeof(struct chunk_t*), 0,
		       (NUMBER_OF_CHUNKS(new_size) - NUMBER_OF_CHUNKS(v->size)) * sizeof(struct chunk_t*));
		/* update structure */
		v->chunks = new_arr;
		v->size = new_size;
	}
	if(v->chunks[IDX_TO_CHUNK(idx)] == NULL)
	{
		/* alloc chunk */
		v->chunks[IDX_TO_CHUNK(idx)] = calloc(1, sizeof(struct chunk_t));
		if(v->chunks[IDX_TO_CHUNK(idx)] == NULL)
			return -4;
	}

	v->chunks[IDX_TO_CHUNK(idx)]->data[IDX_TO_OFFSET(idx)] = value;
	return 0;
}

/* remove single item at given index, optionally call deletor */
int dynvec_remove(struct dynvec_t *v, unsigned int idx, void(*del_func)(unsigned int idx, void* data))
{
	#ifdef DYNVEC_USE_SAFETY_CHECKS
	if(v == NULL)
		return -1;
	if(v->chunks == NULL)
		return -2;
	#endif
	/* check existence */
	if(idx >= v->size)
		return 0;
	if(v->chunks[IDX_TO_CHUNK(idx)] == NULL)
		return 0;

	/* call deletor function if given */
	if((v->chunks[IDX_TO_CHUNK(idx)]->data[IDX_TO_OFFSET(idx)] != NULL) &&
	   (del_func != NULL))
		del_func(idx, v->chunks[IDX_TO_CHUNK(idx)]->data[IDX_TO_OFFSET(idx)]);
	v->chunks[IDX_TO_CHUNK(idx)]->data[IDX_TO_OFFSET(idx)] = NULL;

	/* cleanup chunk if empty */
	for(unsigned int j = 0; j < DYNVEC_CHUNK_SIZE; j++)
	{
		/* break, if non-empty */
		if(v->chunks[IDX_TO_CHUNK(idx)]->data[j] != NULL)
			return 0;
	}
	free(v->chunks[IDX_TO_CHUNK(idx)]);
	v->chunks[IDX_TO_CHUNK(idx)] = NULL;

	return 0;
}

/* count number of elements */
unsigned int dynvec_num_items(struct dynvec_t *v)
{
	unsigned int n = 0;
	unsigned int i;
	unsigned int j;

	#ifdef DYNVEC_USE_SAFETY_CHECKS
	if(v == NULL)
		return -1;
	if(v->chunks == NULL)
		return -2;
	#endif

	/* count non-NULL items */
	for(i = 0; i < NUMBER_OF_CHUNKS(v->size); i++)
	{
		/* skip empty chunks */
		if(v->chunks[i] == NULL)
			continue;

		/* count used items in chunk */
		for(j = 0; j < DYNVEC_CHUNK_SIZE; j++)
		{
			if(v->chunks[i]->data[j] != NULL)
				n++;
		}
	}

	return n;
}

/* find next item */
unsigned int dynvec_next_item(struct dynvec_t *v, unsigned int idx)
{
	unsigned int i;
	unsigned int j;

	#ifdef DYNVEC_USE_SAFETY_CHECKS
	if(v == NULL)
		return -1;
	if(v->chunks == NULL)
		return -2;
	#endif

	/* special case: to be able to find the first item, use DYNVEC_OOB as start */
	if(idx == DYNVEC_OOB)
		idx = 0;
	else
		idx++;

	/* first chunk is started being checked at next sub-index */
	j = IDX_TO_OFFSET(idx);
	for(i = IDX_TO_CHUNK(idx); i < NUMBER_OF_CHUNKS(v->size); i++)
	{
		/* skip empty chunks */
		if(v->chunks[i] == NULL)
			continue;

		/* return next non-empty index */
		for(; j < DYNVEC_CHUNK_SIZE; j++)
		{
			if(v->chunks[i]->data[j] != NULL)
			{
				return i * DYNVEC_CHUNK_SIZE + j;
			}
		}
		/* next chunk is checked from its beginning */
		j = 0;
	}

	/* nothing found -> return index out of bounds */
	return DYNVEC_OOB;
}

#ifdef __TEST_DYNVEC__
#include <stdio.h>
void del_func(unsigned int idx, void* data)
{
	printf("REMOVE: %d, %p, '%s'\n", idx, data, data);
}

int main()
{
	struct dynvec_t dv;
	int i;
	unsigned int n;
	char* testval = "TESTVAL";

	printf("init: %d\n", dynvec_init(&dv, 0));
	printf("\tsize: %d\n", dv.size);
	printf("\titems: %d\n", dynvec_num_items(&dv));
	printf("\tchunks: %p\n", dv.chunks);
	if(dv.chunks != NULL)
		for(i = 0; i < IDX_TO_CHUNK(dv.size); i++)
			printf("\t\t[%d]: %p\n", i, dv.chunks[i]);
	printf("done\n");
	getchar();

	printf("adding 212 entries\n");
	for(i = 0; i < 212; i++)
	{
		if(dynvec_set(&dv, i, testval) != 0)
			printf("\t%d: ERROR\n", i);
	}
	printf("\tsize: %d\n", dv.size);
	printf("\titems: %d\n", dynvec_num_items(&dv));
	printf("\tchunks: %p\n", dv.chunks);
	if(dv.chunks != NULL)
		for(i = 0; i < IDX_TO_CHUNK(dv.size); i++)
			printf("\t\t[%d]: %p\n", i, dv.chunks[i]);
	printf("done\n");
	getchar();

	printf("lookup existing entries\n");
	for(i = 0; i < 212; i++)
	{
		if(dynvec_get(&dv, i) != testval)
			printf("\t%d: ERROR\n", i);
	}
	printf("done\n");
	getchar();

	printf("removing entries: 123 to 159 using deletor\n");
	for(i = 123; i < 160; i++)
	{
		if(dynvec_remove(&dv, i, del_func) != 0)
			printf("\t%d: ERROR\n", i);
	}
	printf("\tsize: %d\n", dv.size);
	printf("\titems: %d\n", dynvec_num_items(&dv));
	printf("\tchunks: %p\n", dv.chunks);
	if(dv.chunks != NULL)
		for(i = 0; i < IDX_TO_CHUNK(dv.size); i++)
			printf("\t\t[%d]: %p\n", i, dv.chunks[i]);
	printf("done\n");
	getchar();

	printf("lookup 255 entries (including non-existing [removed / uncreated])\n");
	for(i = 0; i < 255; i++)
	{
		if(dynvec_get(&dv, i) != testval)
			printf("\t%d: not present\n", i);
	}
	printf("done\n");
	getchar();

	printf("iterating over items\n");
	printf("\tsize: %d\n", dv.size);
	printf("\titems: %d\n", dynvec_num_items(&dv));
	printf("\tchunks: %p\n", dv.chunks);
	n = 0;
	i = DYNVEC_OOB;
	while((i = dynvec_next_item(&dv, i)) != DYNVEC_OOB)
	{
		if(dynvec_get(&dv, i) == testval)
		{
			n++;
			printf("\t%d: item found\n", i);
		}
	}
	printf("\tfound: %d items\n", n);
	printf("done\n");
	getchar();

	printf("delete vector: %d\n", dynvec_delete(&dv, NULL));
	printf("\tsize: %d\n", dv.size);
	printf("\titems: %d\n", dynvec_num_items(&dv));
	printf("\tchunks: %p\n", dv.chunks);
	if(dv.chunks != NULL)
		for(i = 0; i < IDX_TO_CHUNK(dv.size); i++)
			printf("\t\t[%d]: %p\n", i, dv.chunks[i]);
	printf("done\n");
	getchar();

	return 0;
}
#endif
