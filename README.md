Description
===========

A trie is an acyclic, deterministic, finite state automaton. It can be used to store arbitrary data assigned to a key. Lookup and insertion speed is not limited by the dictionary's size but only on the key's length. This allows constant access to the data.

Building
========

The main Makefile in the project's root directory is used to build both - static and dynamic - libraries under Linux. A sample and test program can be found in the sub-directory `test`.

The libraies
------------

Just run `make install` with the appropriate user rights to build and install the library. The procedure also generates the `pkg-config` file.

Some shell implementations behave a bit strange by leaving parts of the command used to generate the `pkg-config` file in its content. In case you got problems running `pkg-config --libs --cflags trie`, just edit `/usr/local/lib/pkgconfig/trie.pc` and remove the prefix `-e ` from the file's content.

The test program
----------------

The `test` directory contains a short `Makefile` that can be used to build the test program. Since the library only contains one source file, you might consider running `gcc -o test test.c ../trie.c -I../` instead. This allows builds on Windows and other platforms where `pkg-config`is not available.
