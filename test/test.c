#ifdef WIN32
#include <windows.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <trie.h>

#ifndef GetTickCount
#include <sys/time.h>
unsigned int GetTickCount()
{
	struct timeval t;

	gettimeofday(&t, NULL);
	return(t.tv_sec * 1000 + t.tv_usec/1000);
}
#endif

int print(char* id, void* info, void* ctx)
{
	if((id == NULL) || (info == NULL))
		return(0);

#ifdef _PRINT
	printf("\"%s\" -> \t0x%p\n", id, info);
#endif
	return(1);
}

static int count = 0;
void delete_cb(void* info)
{
	if(info == NULL)
		return;

	count++;
#ifdef _PRINT
	printf("freeing 0x%p\n", info);
#endif
	free(info);
}

void trie_test(int n)
{
	static char buf[80];
	trie_t *trie;
	void* info;
	int i, p;
	unsigned long t1, t2;

	trie = trie_init();
#ifdef _PRINT
	printf("trie @0x%p\n", trie);
#endif
#ifdef _USER_WAIT
	printf("adding words\n");
	printf("-- press return --\n");
	getchar();
	t1 = GetTickCount();
#endif
	/* fill trie */
	for(i=0; i<n; i++)
	{
		info = malloc(1024);
		if(info == NULL)
			break;
		sprintf(buf, "word%03d", i);
		if(trie_add(trie, buf, info) != 0)
		{
			free(info);
			break;
		}
#ifdef _PRINT
		printf("%03d: added: %s\n", i, buf);
#endif
	}
#ifdef _COUNT
	printf("\n---STATS---\n");
	printf("size node:    %d\n", sizeof(struct node_t));
	printf("size state:   %d\n", sizeof(struct state_t));
	printf("words:        %d\n", count.words);
	printf("chars:        %d\n", count.chars);
	printf("states:       %d\n", count.states);
	printf("transitions:  %d\n", count.transitions);
	printf("node memory:  %d\n", sizeof(struct node_t) * count.transitions);
	printf("state memory: %d\n", sizeof(struct state_t) * count.states);
	printf("---STATS---\n\n");
#endif
#ifdef _USER_WAIT
	t2 = GetTickCount();
	printf("TIME: %dms\n", abs(t2 - t1));
	printf("searching words\n");
	printf("-- press return --\n");
	getchar();
	t1 = GetTickCount();
#endif
	/* lookup each word */
	for(i=0; i<n; i++)
	{
		sprintf(buf, "word%03d", i);
		if(trie_lookup(trie, buf, NULL) != 0)
		{
			printf("%03d: !!! missing !!!: %s\n", i, buf);
			break;
		}
#ifdef _PRINT
		printf("%03d: found: %s\n", i, buf);
#endif
	}
#ifdef _USER_WAIT
	t2 = GetTickCount();
	printf("TIME: %dms\n", abs(t2 - t1));
	printf("testing for-each\n");
	printf("-- press return --\n");
	getchar();
	t1 = GetTickCount();
#endif
	trie_foreach(trie, 0, &print, NULL);
#ifdef _USER_WAIT
	t2 = GetTickCount();
	printf("TIME: %dms\n", abs(t2 - t1));
	printf("deleting trie\n");
	printf("-- press return --\n");
	getchar();
	t1 = GetTickCount();
#endif
	trie_delete(trie, delete_cb);
#ifdef _USER_WAIT
	t2 = GetTickCount();
	printf("TIME: %dms\n", abs(t2 - t1));
	printf("-- press return --\n");
	getchar();
#endif
}

int main(int argc, char** argv)
{
	if(argc > 1)
		trie_test(atoi(argv[1]));
	else
		trie_test(1000);

	printf("Freed %d items\n", count);

	return(0);
}
