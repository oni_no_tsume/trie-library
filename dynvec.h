/*
	dynamic vector
	Author : M.Langer
	Copyright : M.Langer

	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT OWNER AND CONTRIBUTORS AS IS
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __DYNVEC_H__
#define __DYNVEC_H__

#ifndef DYNVEC_CHUNK_SIZE
#define DYNVEC_CHUNK_SIZE  16
#endif

#include <limits.h>

#define DYNVEC_OOB  UINT_MAX

#ifdef __cplusplus
extern "c" {
#endif

struct chunk_t
{
	void* data[DYNVEC_CHUNK_SIZE];
};

struct dynvec_t
{
	struct chunk_t** chunks;
	unsigned int size;
};


/* public functions */
/* init dynamic vector */
int dynvec_init(struct dynvec_t *v, unsigned int size);

/* delete dynamic vector, optionally call deletor on each item */
int dynvec_delete(struct dynvec_t *v, void(*del_func)(unsigned int idx, void* data));

/* retrieve item at given index */
void* dynvec_get(struct dynvec_t *v, unsigned int idx);

/* set item at given index */
int dynvec_set(struct dynvec_t *v, unsigned int idx, void* value);

/* remove single item at given index, optionally call deletor */
int dynvec_remove(struct dynvec_t *v, unsigned int idx, void(*del_func)(unsigned int idx, void* data));

/* count number of elements */
unsigned int dynvec_num_items(struct dynvec_t *v);

/* find next item */
unsigned int dynvec_next_item(struct dynvec_t *v, unsigned int idx);

#ifdef __cplusplus
}
#endif

#endif
