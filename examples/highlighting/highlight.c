#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>

#include <trie.h>

#define DEFAULT_LIST_FILE  "./words.list"
#define INPUT_BUFFER_SIZE  4096
#define WORD_MAX_LEN       80
#define DATA_MAX_LEN       16

#define PARSE_START    0
#define PARSE_KEY      1
#define PARSE_SPACE    2
#define PARSE_ESC_1    3
#define PARSE_ESC_2    4
#define PARSE_DATA     5
#define PARSE_ERROR    254
#define PARSE_SUCCESS  255

#define IS_NL(x)   (((x) == '\n') || ((x) == '\r'))
#define IS_WS(x)   (((x) == ' ') || ((x) == '\t'))

struct config_t
{
	int state;
	char key[WORD_MAX_LEN];
	char data[DATA_MAX_LEN];
	int key_pos;
	int data_pos;
	trie_t *words;
};

void delete_cb(void* info)
{
	if(info == NULL)
		return;

	free(info);
}

int config_parse(struct config_t *cfg, char** buffer, int* len)
{
	char input;

	if((cfg == NULL) || (buffer == NULL) || (len == NULL))
		return PARSE_ERROR;

	if(*buffer == NULL)
		return cfg->state;

	/* consume input */
	while(*len > 0)
	{
		/* re-start parsing when called in state ERROR or SUCCESS */
		if((cfg->state == PARSE_ERROR) || (cfg->state == PARSE_SUCCESS))
			cfg->state = PARSE_START;

		/* reset saved values on START */
		if(cfg->state == PARSE_START)
		{
			cfg->data[0] = '\0';
			cfg->data_pos = 0;
			cfg->key[0] = '\0';
			cfg->key_pos = 0;
			cfg->state = PARSE_KEY;
		}

		input = **buffer;

		switch(cfg->state)
		{
			case PARSE_KEY:
				/* convert key to lowercase */
				if(IS_WS(input))
					cfg->state = PARSE_SPACE;
				else if(IS_NL(input))
					cfg->state = PARSE_SUCCESS;
				else
				{
					cfg->key[cfg->key_pos] = tolower(input);
					cfg->key_pos++;
					/* boundary check */
					if(cfg->key_pos == WORD_MAX_LEN)
						cfg->state = PARSE_ERROR;
					/* ensure key to be nul-terminated */
					cfg->key[cfg->key_pos] = '\0';
				}
				break;
			case PARSE_SPACE:
				/* consume all white spaces here */
				if(IS_NL(input))
					cfg->state = PARSE_SUCCESS;
				else if(input == 'E')
					cfg->state = PARSE_ESC_1;
				else if(!IS_WS(input))
					cfg->state = PARSE_ERROR;
				break;
			case PARSE_ESC_1:
				if(input == 'S')
					cfg->state = PARSE_ESC_2;
				else
					cfg->state = PARSE_ERROR;
				break;
			case PARSE_ESC_2:
				if(input == 'C')
				{
					/* replace "ESC" by character 0x1B */
					cfg->data[0] = '\x1B';
					cfg->data[1] = '\0';
					cfg->data_pos = 1;
					/* move on to data copy */
					cfg->state = PARSE_DATA;
				}
				else
					cfg->state = PARSE_ERROR;
				break;
			case PARSE_DATA:
				if(IS_NL(input))
					cfg->state = PARSE_SUCCESS;
				else
				{
					cfg->data[cfg->data_pos] = input;
					cfg->data_pos++;
					if(cfg->data_pos == DATA_MAX_LEN)
						cfg->state = PARSE_ERROR;
					/* ensure data to be nul-terminated */
					cfg->data[cfg->data_pos] = '\0';
				}
				break;
			default:
				cfg->state = PARSE_ERROR;
		}

		/* adjust input buffer start and length */
		*len -= 1;
		*buffer += 1;

		/* interrupt parsing on ERROR or SUCCESS, caller can handle
		   results before continuing parsing */
		if((cfg->state == PARSE_ERROR) || (cfg->state == PARSE_SUCCESS))
			break;
	}

	return cfg->state;
}

int config_load(struct config_t *cfg, char* filename)
{
	int fd;
	char buffer[INPUT_BUFFER_SIZE];
	char* p;
	char* info;
	ssize_t n = 0;
	int ret = 0;
	int left;
	int i;

	if(filename == NULL)
	{
		printf("No file name given\n");
		return -1;
	}

	fd = open(filename, O_RDONLY);
	if(fd < 0)
	{
		perror("Cannot open file");
		return -2;
	}

	/* fetch input and feed it into the parser */
	while((n = read(fd, buffer, INPUT_BUFFER_SIZE)) > 0)
	{
		p = buffer;
		left = n;
		while(left > 0)
		{
			config_parse(cfg, &p, &left);
			if(cfg->state == PARSE_SUCCESS)
			{
				info = strdup(cfg->data);
				if(trie_add(cfg->words, cfg->key, info) != 0)
					free(info);
			}
			else if(cfg->state == PARSE_ERROR)
			{
				/* print error message */
				printf("Parse error at %d\n\n", n - left);
				for(i = 0; i < n; i++)
				{
					if(i == n - left)
						printf("\x1B[31m");
					printf("%c", buffer[i]);
					if(i == n - left)
						printf("\x1B[0m");
				}
				printf("\n");
				ret = -3;
				goto config_load_end;
			}
		}
	}

config_load_end:
	close(fd);
	return ret;
}

int tokenize(char* source, int len, char** found, int* flen)
{
	char* p;
	int n;

	if((source == NULL) || (found == NULL) || (flen == NULL))
		return -1;

	/* skip leading white spaces (and new-lines) */
	p = source;
	n = 0;
	while((n < len) && (IS_WS(p[n]) || IS_NL(p[n])))
		n++;
	/* mark until next white space or new-line */
	*found = &(p[n]);
	*flen = 0;
	while((n < len) && !(IS_WS(p[n]) || IS_NL(p[n])))
	{
		*flen += 1;
		n++;
	}

	/* return number of input characters consumed */
	return n;
}

int main(int argc, char** argv)
{
	int ret = 0;
	struct config_t cfg;
	void* info;
	char buffer[INPUT_BUFFER_SIZE];
	ssize_t n = 0;
	int i;
	char* found;
	char* last;
	int flen;
	int r;
	int prefix_len;
	int buff_left;
	int has_sequence;

	/* prepare and load word list from config */
	cfg.words = trie_init();
	cfg.state = PARSE_START;
	if(config_load(&cfg, DEFAULT_LIST_FILE) != 0)
	{
		printf("No correct config => exit\n");
		ret = -1;
		goto main_end;
	}

	/* process input from stdin */
	prefix_len = 0;
	buff_left = 0;
	while(1)
	{
		/* 1st: load input */
		if(buff_left == 0)
		{
			/* read next chunk */
			n = read(0, buffer + prefix_len, INPUT_BUFFER_SIZE - prefix_len);
			found = buffer;
			last = buffer;
			flen = 0;
			/* no more input => stop processing */
			buff_left = prefix_len + n;
			if(buff_left <= 0)
				break;
		}

		/* 2nd: run tokenizer over input buffer */
		found += flen;
		r = tokenize(found, buff_left, &found, &flen);
		if(r > 0)
		{
			/* print only token that are not trimmed by end of input chunk */
			if(IS_WS(found[flen]) || IS_NL(found[flen]))
			{
				/* print any chars in between two token */
				for( ;last < found; last++)
					putchar(*last);
				/* lookup token */
				has_sequence = (trie_lookup_substr(cfg.words, found, flen, &info) == 0);
				/* output associated ESC sequence, if any */
				if(has_sequence)
					printf(info);
				/* print token */
				for(i = 0; i < flen; i++)
					putchar(found[i]);
				last = found + flen;
				/* retore changes made by ESC sequence */
				if(has_sequence)
					printf("\x1B[0m");
				buff_left -= r;
			}
		}

		/* 3rd: move any half-handled end of input to start of next buffer */
		prefix_len = 0;
		if(!(IS_WS(found[flen]) || IS_NL(found[flen])))
		{
			prefix_len = flen;
			memcpy(buffer, found, prefix_len);
			buff_left = 0;
		}
	}
	/* print any remaining chars */
	for( ;buff_left > 0; last++, buff_left--)
		putchar(*last);

main_end:
	trie_delete(cfg.words, &delete_cb);
	return ret;
}
