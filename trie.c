/*
	trie data structure implementation
	Author : M.Langer
	Copyright : M.Langer

	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT OWNER AND CONTRIBUTORS �AS IS�
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdlib.h>
#include <string.h>

#include "trie.h"
#include "dynvec.h"

struct node_t
{
	unsigned int index;
	struct state_t* state;
};

struct state_t
{
	struct state_t* back;
	struct dynvec_t adj_list;
	void* info;
};

struct stack_elem_t
{
	struct node_t node;
	int len;
	char* alpha;
};

/* construct new state */
struct state_t* new_state()
{
	struct state_t* state;

	state = malloc(sizeof(struct state_t));
	if(state == NULL)
		return(NULL);

	if(dynvec_init(&state->adj_list, 0) != 0)
	{
		free(state);
		return(NULL);
	}
	state->info = NULL;
	state->back = NULL;
	return(state);
}

/* destruct state */
void delete_state(struct state_t* state)
{
	dynvec_delete(&state->adj_list, NULL);
	free(state);
}

/* find transition in list */
struct state_t* get_transition(struct state_t* state, char symbol)
{
	if(state == NULL)
		return(NULL);

	return(dynvec_get(&state->adj_list, symbol));
}

/* remove transition */
void delete_transition(struct state_t* state, char symbol)
{
	if(state == NULL)
		return;

	dynvec_remove(&state->adj_list, symbol, NULL);
}

/* allocate & init trie structure */
trie_t* trie_init()
{
	return(new_state());
}

/* get value of id */
int trie_lookup(trie_t* trie, char* id, void** value)
{
	struct state_t* state;

	if(id == NULL)
		return(-1);

	state = trie_get_subtrie(trie, id);
	if(state == NULL)
		return(-1);

	if(state->info != NULL)
	{
		if(value != NULL)
			*value = state->info;
		return(0);
	}
	return(-1);
}

int trie_lookup_substr(trie_t* trie, char* id, size_t len, void** value)
{
	struct state_t* state;

	if(id == NULL)
		return(-1);

	state = trie_get_subtrie_substr(trie, id, len);
	if(state == NULL)
		return(-1);

	if(state->info != NULL)
	{
		if(value != NULL)
			*value = state->info;
		return(0);
	}
	return(-1);
}

/* get subtrie of given prefix */
trie_t * trie_get_subtrie(trie_t* trie, char* prefix)
{
	if(prefix == NULL)
		return(NULL);
	return(trie_get_subtrie_substr(trie, prefix, TRIE_STACK_ELEMS));
}

trie_t * trie_get_subtrie_substr(trie_t* trie, char* prefix, size_t len)
{
	struct state_t* state;

	if(prefix == NULL)
		return(NULL);

	state = trie;
	/* iterate over input */
	while((len != 0) && (*prefix != 0))
	{
		/* look for transition */
		state = get_transition(state, *prefix);
		if(state == NULL)
			return(NULL);
		prefix++;
		len--;
	}
	return(state);
}

/* add word and info to trie */
int trie_add(trie_t* trie, char* id, void* info)
{
	if(id == NULL)
		return(-1);
	return(trie_add_substr(trie, id, TRIE_STACK_ELEMS, info));
}

int trie_add_substr(trie_t* trie, char* id, size_t len, void* info)
{
	struct state_t* state;
	struct state_t* next_state;

	if((id == NULL) || (trie == NULL))
		return(-1);

	state = trie;
	/* iterate over input */
	while((len != 0) && (*id != 0))
	{
		/* look for transition */
		next_state = get_transition(state, *id);
		if(next_state == NULL)
		{
			/* create new transition */
			next_state = new_state();
			if(next_state == NULL)
				return(-1);
			/* add transition to the new state */
			if(dynvec_set(&state->adj_list, *id, next_state))
			{
				free(next_state);
				return(-1);
			}
			/* remember back reference */
			next_state->back = state;
		}
		/* move to next state */
		state = next_state;
		id++;
		len--;
	}
	state->info = info;

	return(0);
}

/* remove word and info from trie */
int trie_remove(trie_t* trie, char* id, void(*func)(void* info))
{
	if(id == NULL)
		return(-1);
	return(trie_remove_substr(trie, id, TRIE_STACK_ELEMS, func));
}

int trie_remove_substr(trie_t* trie, char* id, size_t len, void(*func)(void* info))
{
	struct state_t* state;
	struct state_t* last;
	struct state_t* back;
	char last_char = 0;
	int ret_val = -1;

	if((id == NULL) || (trie == NULL))
		return(ret_val);

	last = NULL;
	state = trie;
	/* iterate over input */
	while((len != 0) && (*id != 0))
	{
		/* remember last state with >1 transitions or last final state */
		if(state->info != NULL)
		{
			last = state;
			last_char = *id;
		}
		else if(dynvec_num_items(&state->adj_list) > 1)
		{
			last = state;
			last_char = *id;
		}
		/* look for transition */
		state = get_transition(state, *id);
		if(state == NULL)
			return(ret_val);
		id++;
		len--;
	}

	/* remove final state mark */
	if(state->info != NULL)
	{
		if(func)
			func(state->info);
		state->info = NULL;
		ret_val = 0;
	}

	/* delete path if state is a terminated end */
	if(dynvec_num_items(&state->adj_list) == 0)
	{
		/* delete path till saved state >1 transitions or last final state */
		while(state != last)
		{
			back = state->back;
			delete_state(state);
			state = back;
		}
		/* remove transition leading to state */
		if(last != NULL)
		{
			delete_transition(last, last_char);
		}
	}

	return(ret_val);
}

/* internal stack functions */
int push(struct stack_elem_t** stack, struct stack_elem_t* e)
{
	struct stack_elem_t* top;

	if(stack == NULL)
		return(-1);
	if((*stack == NULL) || (e == NULL))
		return(-1);

	top = *stack;
	top++;
	top->alpha = malloc(e->len + 1);
	if(top->alpha == NULL)
		return(-1);
	memcpy(top->alpha, e->alpha, e->len + 1);
	top->len = e->len;
	top->node.index = e->node.index;
	top->node.state = e->node.state;
	*stack = top;
	return(0);
}

int pop(struct stack_elem_t** stack, struct stack_elem_t* e)
{
	struct stack_elem_t* top;

	if(stack == NULL)
		return(-1);
	if(*stack == NULL)
		return(-1);

	top = *stack;
	if(e != NULL)
	{
		e->len = top->len;
		memcpy(e->alpha, top->alpha, e->len + 1);
		e->node.index = top->node.index;
		e->node.state = top->node.state;
	}
	free(top->alpha);
	top--;
	*stack = top;
	return(0);
}

/* iterate over all words, break on delimiter, calling func */
int trie_foreach(trie_t* trie, char delim, int(*func)(char* id, void* info, void * ctx), void * ctx)
{
	struct state_t* state;
	struct node_t* node;
	struct stack_elem_t* stack;
	struct stack_elem_t* bottom;
	struct stack_elem_t e;
	int ret_val = -1;
	int running = 1;
	unsigned int active_idx;

	if((trie == NULL) || (func == NULL))
		return(ret_val);

	/* break on empty trie */
	if(dynvec_num_items(&trie->adj_list) == 0)
		return(0);

	/* allocate stack */
	bottom = calloc(sizeof(struct stack_elem_t), TRIE_STACK_ELEMS);
	if(bottom == NULL)
		return(ret_val);
	stack = bottom;

	/* get memory for longest possible word */
	e.alpha = malloc(TRIE_STACK_ELEMS);
	if(e.alpha == NULL)
		goto end;
	e.alpha[0] = 0;
	e.len = 0;
	/* push root onto stack */
	e.node.state = trie;
	e.node.index = dynvec_next_item(&trie->adj_list, DYNVEC_OOB);
	if(e.node.index == DYNVEC_OOB)
	{
		/* empty trie => end */
		ret_val = 0;
		goto end;
	}
	if(push(&stack, &e) != 0)
		goto end;

	/* process stack */
	while(stack != bottom)
	{
		/* get active transition from TOS */
		node = &stack->node;
		active_idx = node->index;
		state = dynvec_get(&node->state->adj_list, active_idx);
		/* construct word */
		memcpy(e.alpha, stack->alpha, stack->len + 1);
		e.len = stack->len;
		e.alpha[e.len] = (char)active_idx;
		e.len++;
		e.alpha[e.len] = 0;
		/* call func if state is accepting state or
		   when the delimiter is read */
		if((state->info != NULL) || (e.alpha[e.len-1] == delim))
		{
			/* keep on running while func returns != 0 */
			running = func(e.alpha, state->info, ctx);
			if(running == 0)
				break;
		}
		/* save next transition on TOS */
		node->index = dynvec_next_item(&node->state->adj_list, active_idx);
		/* pop from stack on last transition */
		if(node->index == DYNVEC_OOB)
			if(pop(&stack, NULL) != 0)
				goto end;
		/* advance active transition to next branch */
		while(dynvec_num_items(&state->adj_list) == 1)
		{
			/* move forward, use first (and only) transition */
			active_idx = dynvec_next_item(&state->adj_list, DYNVEC_OOB);
			if(active_idx == DYNVEC_OOB)
				break;
			state = dynvec_get(&state->adj_list, active_idx);
			/* construct word */
			e.alpha[e.len] = (char)active_idx;
			e.len++;
			e.alpha[e.len] = 0;
			/* call func if state is accepting state or
			   when the delimiter is read */
			if((state->info != NULL) || (e.alpha[e.len-1] == delim))
			{
				/* keep on running while func returns != 0 */
				running = func(e.alpha, state->info, ctx);
				if(running == 0)
					break;
			}
		}
		/* push branching state */
		e.node.index = dynvec_next_item(&state->adj_list, DYNVEC_OOB);
		if(e.node.index == DYNVEC_OOB)
			continue;
		e.node.state = state;
		if(push(&stack, &e) != 0)
			goto end;
	}

	ret_val = 0;
end:
	/* free word buffer */
	if(e.alpha != NULL)
		free(e.alpha);
	/* clear & free stack */
	while(stack != bottom)
		pop(&stack, NULL);
	free(bottom);
	return(ret_val);
}

int trie_delete(trie_t* trie, void(*func)(void* info))
{
	struct state_t* state;
	struct node_t* node;
	struct stack_elem_t* stack;
	struct stack_elem_t* bottom;
	struct stack_elem_t e;
	unsigned int active_idx;
	int ret_val = -1;

	if(trie == NULL)
		return(ret_val);

	/* allocate stack */
	bottom = calloc(sizeof(struct stack_elem_t), TRIE_STACK_ELEMS);
	if(bottom == NULL)
		return(ret_val);
	stack = bottom;
	/* get memory for a string of length 1 (last letter processed) */
	e.alpha = malloc(2);
	if(e.alpha == NULL)
		goto end;
	e.alpha[0] = 0;
	e.alpha[1] = 0;
	e.len = 0;

	/* push ROOT onto stack */
	e.node.state = trie;
	e.node.index = DYNVEC_OOB;
	if(push(&stack, &e) != 0)
		goto end;

	/* process stack */
	while(stack != bottom)
	{
		/* 1st: get state from stack */
		node = &stack->node;
		/* advance transition iterator on ToS */
		active_idx = dynvec_next_item(&node->state->adj_list, node->index);
		node->index = active_idx;
		/* pop from stack and free state on last transition */
		if(node->index == DYNVEC_OOB)
		{
			if((node->state->info != NULL) && (func != NULL))
				func(node->state->info);
			delete_state(node->state);
			if(pop(&stack, NULL) != 0)
				goto end;
			/* continue with next element on stack */
			continue;
		}

		/* 2nd: follow selected transition */
		state = dynvec_get(&node->state->adj_list, active_idx);

		/* 3rd: push next state onto stack */
		e.node.index = DYNVEC_OOB;
		e.node.state = state;
		if(push(&stack, &e) != 0)
			goto end;
	}

end:
	/* free word buffer */
	if(e.alpha != NULL)
		free(e.alpha);
	/* clear & free stack */
	while(stack != bottom)
		pop(&stack, NULL);
	free(bottom);
	return(ret_val);
}
