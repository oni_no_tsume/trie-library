/*
	trie data structure implementation
	Author : M.Langer
	Copyright : M.Langer

	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.
	2. Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT OWNER AND CONTRIBUTORS �AS IS�
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
	ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
	LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
	SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
	INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.

	edit by: Jonas
	edit by: W. Gehrhardt
*/

#ifndef __TRIE_H__
#define __TRIE_H__

#ifdef _DEBUG
#include <CMemleak.h>
#endif

#ifndef TRIE_STACK_ELEMS
/* enough for words up to 4096 chars */
#define TRIE_STACK_ELEMS  4096
#endif

#ifdef __cplusplus
extern "c" {
#endif

struct state_t;
typedef struct state_t trie_t;

/* public functions */
/* allocate & init trie structure */
trie_t* trie_init();

/* get value of id */
int trie_lookup(trie_t* trie, char* id, void** value);
int trie_lookup_substr(trie_t* trie, char* id, size_t len, void** value);

/* get trie part starting at given id */
trie_t * trie_get_subtrie(trie_t* trie, char* prefix);
trie_t * trie_get_subtrie_substr(trie_t* trie, char* prefix, size_t len);

/* add word and info to trie */
int trie_add(trie_t* trie, char* id, void* info);
int trie_add_substr(trie_t* trie, char* id, size_t len, void* info);

/* remove word from trie */
int trie_remove(trie_t* trie, char* id, void(*func)(void* info));
int trie_remove_substr(trie_t* trie, char* id, size_t len, void(*func)(void* info));

/* iterate over all words, break on delimiter, calling func */
int trie_foreach(trie_t* trie, char delim, int(*func)(char* id, void* info, void * ctx), void * ctx);

/* destruct trie */
int trie_delete(trie_t* trie, void(*func)(void* info));

#ifdef __cplusplus
}
#endif

#endif
