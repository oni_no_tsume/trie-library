LIBRARYNAME=trie
DESCRIPTION=Dictionary based on an acyclic deterministic finite automaton
VERSION=3.0
SONAME=libtrie.so.3

PREFIX=/usr/local
LIBDIR=$(PREFIX)/lib
INCLUDEDIR=$(PREFIX)/include
PKGCONFIGDIR=$(LIBDIR)/pkgconfig

SOURCES := trie.c dynvec.c
HEADERS := trie.h dynvec.h
OBJECTS := $(patsubst %.c,%.o,$(SOURCES))
PICOBJECTS := $(patsubst %.c,%.lo,$(SOURCES))

EXTRA_CFLAGS := -Wall -W -pedantic -Wshadow -Wextra -std=c99

all: lib$(LIBRARYNAME).a lib$(LIBRARYNAME).so.$(VERSION)

%.o: %.c
	${COMPILE.c} $(EXTRA_CFLAGS) -o $@ $<

%.lo: %.c
	${COMPILE.c} $(EXTRA_CFLAGS) -fPIC -o $@ $<

lib$(LIBRARYNAME).a: ${OBJECTS}
	ar -cru lib$(LIBRARYNAME).a $^

lib$(LIBRARYNAME).so.$(VERSION): ${PICOBJECTS}
	${LINK.o} -shared -Wl,-soname,$(SONAME) -o lib$(LIBRARYNAME).so.$(VERSION) $^

install:
	install -d $(DESTDIR)/$(INCLUDEDIR)
	install -d $(DESTDIR)/$(LIBDIR)
	install -d $(DESTDIR)/$(PKGCONFIGDIR)
	for HEADER in $(HEADERS); do \
		install -m 644 $${HEADER} $(DESTDIR)/$(INCLUDEDIR)/ ; \
	done
	install -m 755 lib$(LIBRARYNAME).a $(DESTDIR)/$(LIBDIR)/
	install -m 755 lib$(LIBRARYNAME).so.$(VERSION) $(DESTDIR)/$(LIBDIR)/
	ln -sf lib$(LIBRARYNAME).so.$(VERSION) $(DESTDIR)/$(LIBDIR)/$(SONAME)
	ln -sf lib$(LIBRARYNAME).so.$(VERSION) $(DESTDIR)/$(LIBDIR)/lib$(LIBRARYNAME).so

	echo -e 'libdir=$(LIBDIR)\nincludedir=$(INCLUDEDIR)\n\nName: $(LIBRARYNAME)\nDescription: $(DESCRIPTION)\nVersion: $(VERSION)\nLibs: -L$${libdir} -l$(LIBRARYNAME)\nCflags: -I$${includedir}' > $(DESTDIR)/$(PKGCONFIGDIR)/$(LIBRARYNAME).pc

clean:
	rm -f $(OBJECTS)
	rm -f $(PICOBJECTS)
	rm -f lib$(LIBRARYNAME).a
	rm -f lib$(LIBRARYNAME).so.$(VERSION)
